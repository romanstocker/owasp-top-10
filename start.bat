cd 02_source_code\owasp-blog-secure
dotnet restore
start /b dotnet watch run

cd ..
cd owasp-blog-insecure
dotnet restore
start /b dotnet watch run

cd ..\..
cd 04_presentation
start /b npm start

PAUSE
