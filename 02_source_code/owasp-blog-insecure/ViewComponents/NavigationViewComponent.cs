using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using owasp_blog_insecure.Data;

namespace owasp_blog_insecure.ViewComponents
{
    public class NavigationViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;

        public NavigationViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var blog = await _context.Blog.ToListAsync();
            return View(blog);
        }
    }
}
