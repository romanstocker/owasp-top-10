using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace owasp_blog_insecure.Models
{
    public class BlogEntry
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Created { get; set; }

        public Blog Blog { get; set; }

        public IdentityUser Author { get; set; }

        public List<Comment> Comments { get; set; }
    }
}
