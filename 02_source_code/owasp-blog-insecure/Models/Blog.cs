using System.Collections.Generic;

namespace owasp_blog_insecure.Models
{
    public class Blog
    {
        public int Id { get; set; }
        public string UrlPart { get; set; }
        public string Title { get; set; }

        public List<BlogEntry> BlogEntries { get; set; }
    }
}