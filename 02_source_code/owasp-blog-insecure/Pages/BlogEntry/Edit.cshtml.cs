using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using owasp_blog_insecure.Data;
using owasp_blog_insecure.Models;

namespace owasp_blog_insecure.Pages_BlogEntry
{
    public class EditModel : PageModel
    {
        private readonly owasp_blog_insecure.Data.ApplicationDbContext _context;

        public EditModel(owasp_blog_insecure.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public BlogEntry BlogEntry { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            BlogEntry = await _context.BlogEntry.FirstOrDefaultAsync(m => m.Id == id);

            if (BlogEntry == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(BlogEntry).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BlogEntryExists(BlogEntry.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool BlogEntryExists(int id)
        {
            return _context.BlogEntry.Any(e => e.Id == id);
        }
    }
}
