using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using owasp_blog_insecure.Data;
using owasp_blog_insecure.Models;

namespace owasp_blog_insecure.Pages_BlogEntry
{
    public class DetailsModel : PageModel
    {
        private readonly owasp_blog_insecure.Data.ApplicationDbContext _context;

        public DetailsModel(owasp_blog_insecure.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public BlogEntry BlogEntry { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sql = "SELECT * FROM BlogEntry WHERE Id='" + id + "'";
            BlogEntry = await _context.BlogEntry.Include(blogEntry => blogEntry.Author)
                    .FromSql(sql)
                    .FirstOrDefaultAsync();

            if (BlogEntry == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
