using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using owasp_blog_insecure.Data;
using owasp_blog_insecure.Models;

namespace owasp_blog_insecure.Pages_BlogEntry
{
    public class IndexModel : PageModel
    {
        private readonly owasp_blog_insecure.Data.ApplicationDbContext _context;

        public IndexModel(owasp_blog_insecure.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<BlogEntry> BlogEntry { get;set; }

        public async Task OnGetAsync(int? blogid)
        {
            if (blogid != null)
            {
                BlogEntry = await _context.BlogEntry.Include(blogEntry => blogEntry.Author).Include(blogEntry => blogEntry.Blog)
                    .Where(be => be.Blog.Id == blogid).ToListAsync();
            }
            else
            {
                BlogEntry = await _context.BlogEntry.Include(blogEntry => blogEntry.Author).Include(blogEntry => blogEntry.Blog)
                    .ToListAsync();
            }
        }
    }
}
