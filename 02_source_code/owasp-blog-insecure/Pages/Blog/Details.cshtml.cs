using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using owasp_blog_insecure.Data;
using owasp_blog_insecure.Models;

namespace owasp_blog_insecure.Pages_Blog
{
    public class DetailsModel : PageModel
    {
        private readonly owasp_blog_insecure.Data.ApplicationDbContext _context;

        public DetailsModel(owasp_blog_insecure.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Blog Blog { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Blog = await _context.Blog.FirstOrDefaultAsync(m => m.Id == id);

            if (Blog == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
