using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using owasp_blog_secure.Data;
using owasp_blog_secure.Models;

namespace owasp_blog_secure.Pages_BlogEntry
{
    public class DetailsModel : PageModel
    {
        private readonly owasp_blog_secure.Data.ApplicationDbContext _context;

        public DetailsModel(owasp_blog_secure.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public BlogEntry BlogEntry { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            BlogEntry = await _context.BlogEntry.Include(blogEntry => blogEntry.Author).FirstOrDefaultAsync(m => m.Id == id);

            if (BlogEntry == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
