using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using owasp_blog_secure.Data;
using owasp_blog_secure.Models;

namespace owasp_blog_secure.Pages_BlogEntry
{
    public class CreateModel : PageModel
    {
        private readonly owasp_blog_secure.Data.ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public CreateModel(owasp_blog_secure.Data.ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public BlogEntry BlogEntry { get; set; }

        public async Task<IActionResult> OnPostAsync(int? blogid)
        {
            if(blogid == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return Page();
            }

            BlogEntry.Created = DateTime.Now;
            BlogEntry.Author = await _userManager.GetUserAsync(HttpContext.User);
            BlogEntry.Blog = await _context.Blog.FirstOrDefaultAsync(b => b.Id == blogid);

            _context.BlogEntry.Add(BlogEntry);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index", new { blogid = blogid });
        }
    }
}
