using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using owasp_blog_secure.Data;
using owasp_blog_secure.Models;

namespace owasp_blog_secure.Pages_Blog
{
    public class DetailsModel : PageModel
    {
        private readonly owasp_blog_secure.Data.ApplicationDbContext _context;

        public DetailsModel(owasp_blog_secure.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Blog Blog { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Blog = await _context.Blog.FirstOrDefaultAsync(m => m.Id == id);

            if (Blog == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
