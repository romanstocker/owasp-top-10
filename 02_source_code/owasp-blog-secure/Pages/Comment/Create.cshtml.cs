using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using owasp_blog_secure.Data;
using owasp_blog_secure.Models;

namespace owasp_blog_secure.Pages_Comment
{
    [Authorize]
    public class CreateModel : PageModel
    {
        private readonly owasp_blog_secure.Data.ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public CreateModel(owasp_blog_secure.Data.ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [BindProperty]
        public Comment Comment { get; set; }

        [BindProperty]
        public int BlogEntryId { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Comment.Author = await _userManager.GetUserAsync(HttpContext.User);
            Comment.BlogEntry = await _context.BlogEntry.Where(be => be.Id == BlogEntryId).FirstOrDefaultAsync();

            _context.Comment.Add(Comment);
            await _context.SaveChangesAsync();

            return RedirectToPage($"/BlogEntry/Details", new { id = Comment.BlogEntry.Id });
        }
    }
}
