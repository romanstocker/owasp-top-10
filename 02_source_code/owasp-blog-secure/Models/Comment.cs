using Microsoft.AspNetCore.Identity;

namespace owasp_blog_secure.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Content { get; set; }

        public BlogEntry BlogEntry { get; set; }

        public IdentityUser Author { get; set; }
    }
}
