namespace owasp_blog_secure.Models
{
    public class Page
    {
        public int Id { get; set; }
        public string UrlPart { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}