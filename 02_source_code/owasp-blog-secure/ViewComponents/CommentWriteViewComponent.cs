using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using owasp_blog_secure.Data;
using owasp_blog_secure.Models;

namespace owasp_blog_secure.ViewComponents
{
    public class CommentWriteViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;

        public CommentWriteViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? blogEntryId)
        {
            var comment = new Comment();

            if (blogEntryId != null)
            {
                var blogEntry = await _context.BlogEntry.Where(be => be.Id == blogEntryId).FirstOrDefaultAsync();
                comment.BlogEntry = blogEntry;
            }
            
            return View(comment);
        }
    }
}
