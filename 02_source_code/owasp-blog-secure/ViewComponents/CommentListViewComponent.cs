using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using owasp_blog_secure.Data;
using owasp_blog_secure.Models;

namespace owasp_blog_secure.ViewComponents
{
    public class CommentListViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;

        public CommentListViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? blogEntryId)
        {
            var commentList = new List<Comment>();

            if (blogEntryId != null)
            {
                commentList = await _context.Comment.Include(comment => comment.BlogEntry).Include(comment => comment.Author)
                    .Where(c => c.BlogEntry.Id == blogEntryId).ToListAsync();
            }

            return View(commentList);
        }
    }
}
