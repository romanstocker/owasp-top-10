using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using owasp_blog_secure.Models;

namespace owasp_blog_secure.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Blog> Blog { get; set; }
        public DbSet<BlogEntry> BlogEntry { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Page> Page { get; set; }
    }
}
