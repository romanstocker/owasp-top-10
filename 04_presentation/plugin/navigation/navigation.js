var RevealNavigation = window.RevealNavigation || (function () {
  loadResource("plugin/navigation/navigation.css", "stylesheet");

  initialize();

  function initialize() {
    var reveal = document.querySelector(".contrast");

    var div = document.createElement("div");
    div.className = "navigation";

    var ul = document.createElement("ul");
    ul.className = "navigation-list";
    updateNavigation(ul);

    Reveal.addEventListener("slidechanged", function () {
      updateNavigation(ul);
    });

    div.appendChild(ul);
    reveal.appendChild(div);
  }

  function updateNavigation(ul) {
    var totalSlides = Reveal.getTotalSlides();

    // Reset all child element
    while (ul.firstChild) {
      ul.removeChild(ul.firstChild);
    }

    for (counter = 1; counter <= totalSlides; counter++) {
      var li = document.createElement("li");

      if (Reveal.getSlidePastCount() + 1 === counter || Reveal.getSlidePastCount() === Reveal.getTotalSlides() && counter === 0) {
        li.className = "navigation-list-element active";
      } else {
        li.className = "navigation-list-element";
      }

      ul.appendChild(li);
    }
  };

  // Modified from math plugin
  function loadResource(url, type, callback) {
    var head = document.querySelector("head");
    var resource;

    if (type === "script") {
      resource = document.createElement("script");
      resource.type = "text/javascript";
      resource.src = url;
    } else if (type === "stylesheet") {
      resource = document.createElement("link");
      resource.rel = "stylesheet";
      resource.href = url;
    }

    // Wrapper for callback to make sure it only fires once
    var finish = function () {
      if (typeof callback === "function") {
        callback.call();
        callback = null;
      }
    };

    resource.onload = finish;

    // IE
    resource.onreadystatechange = function () {
      if (this.readyState === "loaded") {
        finish();
      }
    };

    // Normal browsers
    head.appendChild(resource);
  }
})();
