var RevealFooter = window.RevealFooter || (function () {
  loadResource("node_modules/moment/moment.js", "script", function () {
    loadResource("node_modules/moment/locale/de-ch.js", "script", function () {
      loadResource("plugin/footer/footer.css", "stylesheet");

      initialize();

      function initialize() {
        var reveal = document.querySelector(".reveal");

        var footer = document.createElement("footer");
        footer.setAttribute("id", "footer");
        setFooterVisibility(footer);

        // Show date in left column
        var left = document.createElement("div");
        left.className = "footer-left";
        left.innerText = moment().format("LL");
        footer.appendChild(left);

        // Show name in center column
        var center = document.createElement("div");
        center.className = "footer-center";
        center.innerText = "Roman Stocker";
        footer.appendChild(center);

        // Show current slide number and slide number count in right column
        var right = document.createElement("div");
        right.className = "footer-right";

        // Refresh current slide number when slide changes
        Reveal.addEventListener("slidechanged", function () {
          right.innerText =
            Reveal.getSlidePastCount() + 1 + " / " + Reveal.getTotalSlides();

          setFooterVisibility(footer);
        });

        footer.appendChild(right);
        reveal.appendChild(footer);
      }
    });
  });

  function setFooterVisibility(footer) {
    if (Reveal.getSlidePastCount() === 0) {
      footer.className = "hidden";
    } else {
      footer.className = "";
    }
  }

  // Modified from math plugin
  function loadResource(url, type, callback) {
    var head = document.querySelector("head");
    var resource;

    if (type === "script") {
      resource = document.createElement("script");
      resource.type = "text/javascript";
      resource.src = url;
    } else if (type === "stylesheet") {
      resource = document.createElement("link");
      resource.rel = "stylesheet";
      resource.href = url;
    }

    // Wrapper for callback to make sure it only fires once
    var finish = function () {
      if (typeof callback === "function") {
        callback.call();
        callback = null;
      }
    };

    resource.onload = finish;

    // IE
    resource.onreadystatechange = function () {
      if (this.readyState === "loaded") {
        finish();
      }
    };

    // Normal browsers
    head.appendChild(resource);
  }
})();
