var RevealTableOfContents = window.RevealTableOfContents || (function() {
  loadResource("plugin/table-of-contents/table-of-contents.css", "stylesheet");

  initialize();

  function initialize() {
    Reveal.addEventListener("ready", function () {
      var slides = Reveal.getSlides();

      var tableofcontents = document.querySelector("#tableofcontents");

      var ul = document.createElement("ul");

      // Select all slides with a title for the table of contents
      var slides = slides.filter(x => x.title !== "");

      for (let counter = 0; counter < slides.length; counter++) {
        var li = document.createElement("li");

        // Add attributes for use reveal.js fragment functionality
        li.className = "fragment";
        li.setAttribute("data-fragment-index", counter);
        li.innerText = slides[counter].title;

        ul.appendChild(li);
      }

      tableofcontents.appendChild(ul);
    });
  }

  // Modified from math plugin
  function loadResource(url, type, callback) {
    var head = document.querySelector("head");
    var resource;

    if (type === "script") {
      resource = document.createElement("script");
      resource.type = "text/javascript";
      resource.src = url;
    } else if (type === "stylesheet") {
      resource = document.createElement("link");
      resource.rel = "stylesheet";
      resource.href = url;
    }

    // Wrapper for callback to make sure it only fires once
    var finish = function () {
      if (typeof callback === "function") {
        callback.call();
        callback = null;
      }
    };

    resource.onload = finish;

    // IE
    resource.onreadystatechange = function () {
      if (this.readyState === "loaded") {
        finish();
      }
    };

    // Normal browsers
    head.appendChild(resource);
  }
})();
